# Electrophysiology signal basic analysis and visualization

This package is mainly intended for visualizing large eletrophysiology recordings.
Scripts are included to convert several common signal format to a .zarr store that the
visualizing interface is using.
The .zarr store contains a "pyramid" that maintain signal at different resolutions.
This avoids loading the full dataset at once and greatly speeds up the display.
For instance, using this interface, you can visualize datasets over a slow network
connection.

## Installation

This is a python package that installs with common tools (i.e. `pip`).

I do not publish this package on Pypi and you should use a like on `gitlab`.

I recommend using a python virtualenv for installation.
Once you have set up your virtualenv, you install the package with:
```
pip install anaephys --index-url https://gitlab.com/api/v4/projects/43104028/packages/pypi/simple
```

## Dataset structure

You should place your dataset inside a folder and the raw recordings inside a
`raw` folder.
If the dataset is in the `.nwb` format, you should place your file inside a `nwb`
subfolder, etc.

```
dataset/
├── processed
│   └── ephys.zarr
│       ├── lfps
│       │   ├── adcs
│       │   ├── channels
│       │   └── timestamps
│       └── spectro
│           ├── 2048
│           │   ├── freqs
│           │   ├── logspectro
│           │   └── times
│           └── 256
│               ├── freqs
│               ├── logspectro
│               └── times
└── raw
    ├── dataset.zarr
    │   ├── adcs
    │   │   └── 0.0
    │   └── channels
    │       └── 0.0
    └── nwb
        └── dataset_name.nwb
```


## Converting the datasets

First convert your dataset to the .zarr format with:
```
convert --nwbtozarr dataset/
```

see `convert --help` for the options.

## Computing the LFPs and spectrograms

You then compute LFPs, spectrograms and scalograms (if necessary).
```
lfp --lfp --spectro dataset/
```

see `lfp --help` for the options.


## Visualizing the dataset

Finally, you launch:
```
visualize dataset/
```
