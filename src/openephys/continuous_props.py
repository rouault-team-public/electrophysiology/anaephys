"""Read the openephys continuous files and properties."""
import logging
import re
from pathlib import Path

from defusedxml import ElementTree as ETree
from natsort import natsorted

from openephys import OpenEphys


def extract_channels(conti_path: Path) -> tuple:
    """Extract all the .continuous filenames.

    Args:
        conti_path (Path): path containing the .openephys file

    Returns:
        tuple: the path list for channels and adcs
    """
    tree = ETree.parse(conti_path / "Continuous_Data.openephys")
    root = tree.getroot()

    chan_files = []
    adc_files = []
    chan_match = re.compile("^CH")
    adc_match = re.compile("^ADC")
    for chan in root.findall("./RECORDING/PROCESSOR/CHANNEL"):
        attribs = chan.attrib
        if chan_match.match(attribs["name"]):
            chan_files.append(attribs["filename"])
        if adc_match.match(attribs["name"]):
            adc_files.append(attribs["filename"])

    # list(set()) removes duplicates from the lists
    chan_files = natsorted(list(set(chan_files)))
    adc_files = natsorted(list(set(adc_files)))
    n_file = len(chan_files)
    logging.info("Found %d channels", n_file)
    return chan_files, adc_files


def extract_prop(conti_path: Path) -> dict:
    """Extract of the dataset properties into a dictionary.

    Args:
        conti_path (Path): path containing the .openephys file

    Returns:
        dict: the property dictionary.
    """
    dataset_prop = {}
    chan_files, adc_files = extract_channels(conti_path)
    dataset_prop["chan_files"] = chan_files
    dataset_prop["adc_files"] = adc_files

    dataset_prop["n_chan"] = len(chan_files)
    dataset_prop["n_adc"] = len(adc_files)

    logging.info("Opening the first dataset")
    # opening the first file to determine the size of the recordings and
    # getting the timestamps
    data_first = OpenEphys.load(str(conti_path / chan_files[0]))

    dataset_prop["sample_rate"] = float(data_first["header"]["sampleRate"])
    dataset_prop["conv_factor"] = float(data_first["header"]["bitVolts"])

    dataset_prop["tot_len"] = data_first["data"].size

    return dataset_prop
