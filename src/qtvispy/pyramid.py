"""Define a visualization pyramid."""
import logging

import numpy as np
import zarr
from vispy import scene


class HorizontalScaler:
    """Manage the pyramid horizontal scaling.

    Attributes:
        cursor (TYPE): Description
        hyste (float): Description
        scales (TYPE): Description
        subsamp (TYPE): Description
        viewbox (TYPE): Description
    """

    hyste = 1.5

    def __init__(
        self,
        viewbox: scene.ViewBox,
        scales: list,
        t_ranges: zarr.hierarchy.Group,
        raw_sr: float,
    ) -> None:
        """Initialize the viewbox with a low res curve."""
        super().__init__()

        self.xrange = (0, 0)
        self._subsamp = -1
        self._t_ranges = t_ranges
        self._raw_sr = raw_sr
        self.viewbox = viewbox
        self.scales = scales

        viewbox.camera.update_horizontal((t_ranges["256"][0], t_ranges["256"][-1]))
        self.cursor = scene.InfiniteLine(
            0.0, [1.0, 0.0, 0.0, 1.0], parent=viewbox.scene
        )

    @property
    def subsamp(self):
        """Define the subsampling factor."""
        return self._subsamp

    @subsamp.setter
    def subsamp(self, subsamp):
        if subsamp != self._subsamp:
            logging.info("Changing the subsampling: %d", subsamp)
            self._subsamp = subsamp
            self.viewbox.update_dataset()

    def update_horizontal(self, xlim):
        """Check if dataset update is needed and update camera."""
        self.cursor.set_data((xlim[0] + xlim[1]) * 0.5 - self.viewbox.camera.xstart)
        self.update_subsamp(xlim)
        xr0 = self.xrange[0]
        xr1 = self.xrange[1]
        if xr0 > xlim[0] and xr0 > self._t_ranges["256"][0]:
            self.viewbox.update_dataset()
        if xr1 < xlim[1] and xr1 < self._t_ranges["256"][1]:
            self.viewbox.update_dataset()
        self.viewbox.camera.update_horizontal(xlim)

    def update_subsamp(self, xlim: tuple[float, float]):
        """Change the subsampling if necessary.

        Args:
            xlim (tuple[float, float]): the time limits in seconds
        """
        old_diff = self.viewbox.camera.rect.width
        diff = xlim[1] - xlim[0]

        # zooming in:
        if diff < old_diff:
            self.zoom_in(diff, old_diff)
        # zooming out:
        elif diff > old_diff:
            self.zoom_out(diff, old_diff)

    def zoom_in(self, diff: float, old_diff: float):
        """Zoom in and update the subsampling.

        Args:
            diff (float): Description
            old_diff (float): Description
        """
        for i_sca, sca in enumerate(self.scales[1:]):
            if (
                self.subsamp == sca.subsamp
                and diff <= self.scales[i_sca].thr <= old_diff
            ):
                self.subsamp = self.scales[i_sca].subsamp
                break

    def zoom_out(self, diff: float, old_diff: float):
        """Zoom out and update the subsampling.

        Args:
            diff (float): Description
            old_diff (float): Description
        """
        for i_sca, sca in enumerate(self.scales[:-1]):
            if (
                self.subsamp == sca.subsamp
                and old_diff <= sca.thr * HorizontalScaler.hyste <= diff
            ):
                self.subsamp = self.scales[i_sca + 1].subsamp
                break

    def ind_lims(self, xlim: tuple[float, float]) -> tuple[int, int, np.ndarray]:
        """Compute the index limits.

        Args:
            xlim (tuple[float, float]): the time limits in seconds

        Returns:
            tuple[int, int, np.ndarray]: the index limits and time range
        """
        if self.subsamp == 256:
            ind_min = 0
            ind_max = self._t_ranges["256"].size
            t_range = self._t_ranges["256"]
        elif self.subsamp > 0:
            ind_min = int(xlim[0] * 1250 - self.subsamp * 12500)
            ind_max = int(xlim[1] * 1250 + self.subsamp * 12500)

            ind_min = ind_min // self.subsamp
            ind_max = ind_max // self.subsamp
            ind_min = max(0, ind_min)
            ind_max = min(self._t_ranges[str(self.subsamp)].size, ind_max)
            t_range = self._t_ranges[str(self.subsamp)][ind_min:ind_max]
        else:
            ind_min = int(xlim[0] * self._raw_sr - int(self._raw_sr / 2))
            ind_min = max(0, ind_min)
            ind_max = int(xlim[1] * self._raw_sr + int(self._raw_sr / 2))
            ind_max = min(self._t_ranges["1"][-1] * self._raw_sr, ind_max)
            t_range = np.arange(ind_min, ind_max) / self._raw_sr

        return ind_min, ind_max, t_range
