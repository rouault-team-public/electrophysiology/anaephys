"""Define the vispy cameras."""
import numpy as np
from vispy import geometry, scene


class XYZoomCamera(scene.cameras.PanZoomCamera):
    """Pan and zoom in x and y (with modifier keys)."""

    def __init__(self) -> None:
        """Add the definition of xstart."""
        super().__init__()

        self.xstart = 0.0

    def viewbox_mouse_event(self, event) -> None:
        """Update the viewbox when receiving a mouse event.

        Args:
            event (Event): Instance of Event
        """
        if event.handled or not self.interactive:
            return

        # Scrolling
        scene.cameras.BaseCamera.viewbox_mouse_event(self, event)

        modifiers = event.mouse_event.modifiers
        if event.type == "mouse_wheel":
            if modifiers:
                center = self._scene_transform.imap(event.pos)
                self.zoom(
                    (1 + self.zoom_factor) ** (-event.delta[1] * 30),
                    axis=1,
                    center=center,
                )
                event.handled = True
            else:
                center = self._scene_transform.imap(event.pos)
                self.zoom(
                    (1 + self.zoom_factor) ** (-event.delta[1] * 30),
                    center=center,
                )
                event.handled = True

        # Moving
        elif event.type == "mouse_move":
            if event.press_event is None:
                return

            # Translate
            p1 = event.mouse_event.press_event.pos
            p2 = event.mouse_event.pos
            p1 = np.array(event.last_event.pos)[:2]
            p2 = np.array(event.pos)[:2]
            p1s = self._transform.imap(p1)
            p2s = self._transform.imap(p2)
            if modifiers:
                self.pan(p1s - p2s, axis=1)
            else:
                self.pan(p1s - p2s)
            event.handled = True

        elif event.type == "mouse_press":
            event.handled = True
        else:
            event.handled = False

    def zoom(self, factor, axis=0, center=None) -> None:
        """Zoom in (or out) at the given center.

        Args:
            factor (float or tuple): Fraction by which the scene should be
                zoomed (e.g. a factor of 2 causes the scene to appear twice as
                large).
            axis (int): Axis along which the zoom is performed.
            center (tuple of 2-4 elts) : The center of the view. If not given
                or None, use the current center.
        """
        # Init some variables
        center = center if (center is not None) else self.center

        # Get space from given center to edges
        if axis == 0:
            left_space = center[0] - self.rect.left
            right_space = self.rect.right - center[0]
            self.canvas.xlim = (
                center[0] - left_space * factor + self.xstart,
                center[0] + right_space * factor + self.xstart,
            )
        elif axis == 1:
            bottom_space = center[1] - self.rect.bottom
            top_space = self.rect.top - center[1]
            self.update_vertical(
                (
                    center[1] - bottom_space * factor,
                    center[1] + top_space * factor,
                )
            )

    def pan(self, *pan, axis=0) -> None:
        """Pan the view.

        Args:
            pan (tuple): length-2 sequence. The distance to pan the view, in
                the coordinate system of the scene.
            axis (int): Axis along which the zoom is performed.
        """
        if len(pan) == 1:
            pan = pan[0]
        if axis == 0:
            self.canvas.xlim = (
                self.rect.left + pan[0] + self.xstart,
                self.rect.right + pan[0] + self.xstart,
            )
        elif axis == 1:
            self.update_vertical((self.rect.bottom + pan[1], self.rect.top + pan[1]))

    def update_horizontal(self, xlim) -> None:
        """Update the horzontal limits of the zoom."""
        rect = geometry.Rect(self.rect)
        # Scale these spaces
        rect.left = xlim[0] - self.xstart
        rect.right = xlim[1] - self.xstart
        self.rect = rect

    def update_vertical(self, ylim) -> None:
        """Update the horzontal limits of the zoom."""
        rect = geometry.Rect(self.rect)
        # Scale these spaces
        rect.bottom = ylim[0]
        rect.top = ylim[1]
        self.rect = rect
