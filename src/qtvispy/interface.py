"""Define the visualization interface using vispy."""

import collections
import logging
from typing import Any

import numpy as np
import zarr
from matplotlib import cm
from numpy import typing as npt
from PySide6 import QtWidgets
from PySide6.QtCore import Qt
from superqt import QDoubleRangeSlider
from vispy import scene

from qtvispy import cameras, pyramid, vispyarray


class MainWindow(QtWidgets.QMainWindow):
    """Defines the full windows with Qt widgets."""

    def __init__(self) -> None:
        """Initialize the Qt grid."""
        super().__init__()

        self.setWindowTitle("Behavior and EPhys")
        self.setGeometry(16, 32, 1536, 1024)

        layout = QtWidgets.QHBoxLayout()

        self.vispy_canvas = VispyWindow(parent_win=self)
        layout.addWidget(self.vispy_canvas.native)

        right_panel = QtWidgets.QWidget()
        self.right_layout = QtWidgets.QVBoxLayout()
        self.right_layout.setSpacing(0)
        right_panel.setLayout(self.right_layout)
        right_panel.setFixedWidth(120)

        layout.addWidget(right_panel)

        widget = QtWidgets.QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)


class EPhysWidget(scene.ViewBox):
    """Defines a ephys curves vispy viewbox."""

    def __init__(
        self, sub_dat, sub_time, raw_dat=None, vtype: str = "ephys", mapping=None
    ) -> None:
        """Initialize the viewbox with a low res curve."""
        super().__init__(camera=cameras.XYZoomCamera())

        self.height_min = 50

        self.unfreeze()
        self._vtype = vtype
        if vtype == "ephys":
            self.raw_data = raw_dat["channels"]
            if "conv_factor" in raw_dat.attrs:
                self._conv_fact = np.array(raw_dat.attrs["conv_factor"])
            else:
                self._conv_fact = np.array([1.0])
        else:
            self.height_max = 200
            self.raw_data = raw_dat["adcs"]
            if "conv_factor_adc" in raw_dat.attrs:
                self._conv_fact = np.array(raw_dat.attrs["conv_factor_adc"])
            else:
                self._conv_fact = np.array([1.0])
        self.sub_data = sub_dat
        self.median_filter = False
        self.mapping = mapping
        self._lines = scene.Line((0, 0), parent=self.scene, method="gl", color="white")
        ScaleTuple = collections.namedtuple("ScaleTuple", "subsamp, thr")
        scales = (
            ScaleTuple(-1, 0.5),
            ScaleTuple(1, 10.0),
            ScaleTuple(16, 200.0),
            ScaleTuple(256, np.inf),
        )
        self._xscaler = pyramid.HorizontalScaler(
            self, scales, sub_time, raw_dat.attrs["sample_rate"]
        )
        self._yscaler = VerticalScaler(self, vtype)
        ylims = self._yscaler.reset_lims(self.sub_data["1"].shape[0])
        self.camera.update_vertical(ylims)

        self._xscaler.subsamp = 256

    def update_horizontal(self, xlim: tuple[float, float]):
        """Update the view horizontally.

        It simply transmits the limits to the xscaler.

        Args:
            xlim (tuple[float, float]): the horizontal limits in seconds
        """
        self._xscaler.update_horizontal(xlim)

    def update_dataset(self):
        """Update the dataset when the limits are reached."""
        logging.debug("Updating the dataset")
        xlim = np.array((self.camera.rect.left, self.camera.rect.right))
        xlim += self.camera.xstart
        ind_min, ind_max, t_range = self._xscaler.ind_lims(xlim)
        substr = str(self._xscaler.subsamp)
        if self._xscaler.subsamp > 0:
            dat_line = self.sub_data[substr][:, ind_min:ind_max]
        else:
            dat_line = (
                self.raw_data[:, ind_min:ind_max].astype(np.float64)
                * self._conv_fact[:, None]
            )

        if self.mapping is not None:
            dat_line = dat_line[self.mapping]
        self._xscaler.xrange = (t_range[0], t_range[-1])
        self.camera.xstart = xlim[0]
        t_range -= xlim[0]

        self.camera.update_horizontal(xlim)
        if self.median_filter:
            dat_line -= np.median(dat_line, axis=0)

        line, connect = self._yscaler.scale_dataset(t_range, dat_line)

        color = np.ones((dat_line.shape[0], dat_line.shape[1], 4), dtype=np.float32)
        cmap = cm.get_cmap("Pastel2", lut=self.sub_data["1"].shape[0])
        for i in range(dat_line.shape[0]):
            color[i] = np.array(cmap(i / dat_line.shape[0])).reshape((1, 4))
        color = color.reshape((-1, 4))

        self._lines.set_data(line, connect=connect, color=color)

    def update_filter(self, state):
        """Update the dataset when the limits are reached."""
        self.median_filter = Qt.CheckState(state) == Qt.CheckState.Checked
        self.update_dataset()

    def update_offset(self, offset):
        """Update the dataset offset."""
        self._yscaler.offset = float(offset)


class VerticalScaler:
    """Manage the vertical limits of the views.

    Attributes:
        scale (float): the scaling of the dataset
        view (scene.ViewBox): the viewbox
    """

    def __init__(self, view: scene.ViewBox, vtype: str = "ephys"):
        """Initialize the viewbox with a low res curve."""
        super().__init__()

        if vtype == "ephys":
            self._offset = 0.1
            self.scale = 1e-3
            self._sgroup = 8
            self._wgroup = 2.0
        else:
            self._offset = 5.0
            self.scale = 1.0
            self._sgroup = 1
            self._wgroup = 0.5

        self.view = view

    @property
    def offset(self):
        """Define the y-offset between the lines."""
        return self._offset

    @offset.setter
    def offset(self, offset):
        if offset != self._offset:
            logging.info("Updating the offset")
            ratio = offset / self._offset
            self._offset = offset
            self.view.update_dataset()
            rect = self.view.camera.rect
            self.view.camera.update_vertical((rect.bottom * ratio, rect.top * ratio))

    def reset_lims(self, n_chan: int) -> tuple[float, float]:
        """Reset to default vertical limits.

        Args:
            n_chan (int): the number of displayed channels

        Returns:
            tuple[float, float]: the limits
        """
        return (
            -self.offset,
            self.offset * (n_chan + self._wgroup * (n_chan - 1) // self._sgroup),
        )

    def scale_dataset(
        self, trange: np.ndarray, dataset: npt.ArrayLike
    ) -> tuple[np.ndarray, np.ndarray]:
        """Scale a dataset for display.

        Args:
            trange (np.ndarray): Description
            dataset (npt.ArrayLike): Description

        Returns:
            tuple[np.ndarray, np.ndarray]: the line and connection arrays
        """
        n_chan = dataset.shape[0]
        n_time = dataset.shape[1]

        array_line = np.zeros((n_time * n_chan, 2), dtype=np.float32)
        array_line[:, 0] = np.tile(trange, n_chan)
        chans = np.arange(n_chan)[:, np.newaxis]
        array_line[:, 1] = (
            dataset * self.scale
            + self.offset * (chans + self._wgroup * (chans // self._sgroup))
        ).flatten()

        connect = np.ones((n_chan, n_time), dtype=bool)
        connect[:, -1] = False
        connect = connect.flatten()[:-1]

        return array_line, connect


class RasterWidget(scene.ViewBox):
    """Defines a ephys curves vispy viewbox."""

    def __init__(self, spike_times, control_bar, orders, infos, sample_rate):
        """Initialize the viewbox with a low res curve."""
        super().__init__(cameras.XYZoomCamera())

        self.height_min = 150
        self.height_max = 200

        self.unfreeze()

        self._sample_rate = sample_rate
        self.orders = orders
        self.infos = infos
        control_bar.addSpacing(10)
        control_bar.addWidget(QtWidgets.QLabel("Raster"))
        raster_combo = QtWidgets.QComboBox()
        control_bar.addWidget(raster_combo)
        for order in orders.keys():
            raster_combo.addItem(order)
        raster_combo.currentTextChanged.connect(self.update_order)

        self.spike_times = spike_times

        n_cell = len(spike_times.keys())

        coord, connect, colors = vispyarray.generate_raster_line(
            spike_times, self.orders["default"], sample_rate
        )
        self.lines = scene.Line(
            coord,
            connect=connect,
            parent=self.scene,
            method="gl",
            color="white",
        )
        # Draw the cursor in red
        self.cursor = scene.InfiniteLine(0.0, [1.0, 0.0, 0.0, 1.0], parent=self.scene)
        self.camera.update_vertical((-1, n_cell + 1))

    def update_horizontal(self, xlim):
        """Check if dataset update is needed and update camera."""
        self.cursor.set_data((xlim[0] + xlim[1]) * 0.5 - self.camera.xstart)
        self.camera.update_horizontal(xlim)

    def update_order(self, order_name: str):
        """Change the order of the cells after the combobox selection.

        Args:
            order_name (str): the name of the order
        """
        logging.info("Loading the cell order: %s", order_name)
        order = self.orders[order_name]
        info = self.infos[order_name]
        coord, connect, colors = vispyarray.generate_raster_line(
            self.spike_times, order, self._sample_rate, info
        )
        n_cell = len(order)
        if self.lines is not None:
            self.lines.parent = None
        if colors is not None:
            self.lines = scene.Line(
                coord,
                connect=connect,
                parent=self.scene,
                method="gl",
                color=colors,
            )
        else:
            self.lines = scene.Line(
                coord,
                connect=connect,
                parent=self.scene,
                method="gl",
                color="white",
            )
        self.camera.update_vertical((-1, n_cell + 1))


class ScaloWidget(scene.ViewBox):
    """Defines a scalogram vispy viewbox."""

    def __init__(self, scalo, control_bar):
        """Initialize the viewbox with a low res curve."""
        super().__init__(camera=cameras.XYZoomCamera())

        self.unfreeze()
        self.scalo = scalo
        control_bar.addSpacing(10)
        control_bar.addWidget(QtWidgets.QLabel("Scalogram"))
        self.height_max = 200

        self.cursor = scene.InfiniteLine(0.0, [1.0, 0.0, 0.0, 1.0], parent=self.scene)
        # put the cursor on top
        self.cursor.order = 100

        self.color_slider = QDoubleRangeSlider(Qt.Horizontal)
        control_bar.addWidget(self.color_slider)
        self.color_slider.valueChanged.connect(self.update_clim)

        logging.info("Displaying the scalogram")

        t_range = scalo["times"]
        freqs = scalo["freqs"]
        self.viss = vispyarray.generate_textures(scalo["logscalo"], t_range, freqs)

        pow_min = np.min(scalo["logscalo"])
        pow_max = np.max(scalo["logscalo"])
        clim = (pow_min, pow_max)
        for spe in self.viss:
            spe.parent = self.scene

        self.update_clim(clim)

        self.color_slider.setMinimum(clim[0])
        self.color_slider.setMaximum(clim[1])
        self.color_slider.setValue(clim)

        self.camera.update_vertical((freqs[0], freqs[-1]))

    def update_horizontal(self, xlim):
        """Check if dataset update is needed and update camera."""
        self.cursor.set_data(-self.camera.xstart + (xlim[0] + xlim[1]) * 0.5)
        self.camera.update_horizontal(xlim)

    def update_clim(self, cvals):
        """Update the color map limits."""
        for vis in self.viss:
            vis.clim = cvals


class SpectroWidget(scene.ViewBox):
    """Defines a spectrogram vispy viewbox."""

    def __init__(self, spectros, control_bar):
        """Initialize the viewbox with a low res curve."""
        super().__init__(camera=cameras.XYZoomCamera())

        self.unfreeze()
        self.spectros = spectros
        control_bar.addSpacing(10)
        control_bar.addWidget(QtWidgets.QLabel("Spectrogram"))
        spectro_combo = QtWidgets.QComboBox()
        control_bar.addWidget(spectro_combo)
        spectro_names = list(spectros.items())
        for spe in spectro_names:
            spectro_combo.addItem(spe[0])
        spectro_combo.currentTextChanged.connect(self.update_spectro)

        self.height_max = 200

        self.cursor = scene.InfiniteLine(0.0, [1.0, 0.0, 0.0, 1.0], parent=self.scene)
        # put the cursor on top
        self.cursor.order = 100

        self.color_slider = QDoubleRangeSlider(Qt.Horizontal)
        control_bar.addWidget(self.color_slider)
        self.color_slider.valueChanged.connect(self.update_clim)

        logging.info("Displaying the spectrogram")
        self.spectro_viss = None
        spectro_combo.setCurrentIndex(0)
        self.update_spectro(spectro_combo.currentText())

    def update_spectro(self, spectro_name: str):
        """Change the spectrogram after the combobox selection.

        Args:
            spectro_name (str): the name of the spectrogram (window size)
        """
        # GL displays a bug when trying to display large image because a limit
        # on texture size
        width_unit = 8192
        cur_spectro = self.spectros[spectro_name]
        t_range = cur_spectro["times"]
        freqs = cur_spectro["freqs"]
        n_text = (t_range.size + width_unit - 1) // width_unit
        logging.info("Generate %d textures for the spectro", n_text)

        pow_min = np.min(cur_spectro["logspectro"])
        pow_max = np.max(cur_spectro["logspectro"])
        if self.spectro_viss is not None:
            for spe in self.spectro_viss:
                spe.parent = None

        self.spectro_viss = vispyarray.generate_textures(
            cur_spectro["logspectro"], t_range, freqs
        )
        for spe2 in self.spectro_viss:
            spe2.parent = self.scene

        self.color_slider.setMinimum(pow_min)
        self.color_slider.setMaximum(pow_max)
        self.color_slider.setValue((pow_min, pow_max))

        freqs = cur_spectro["freqs"]
        self.camera.update_vertical((freqs[0], freqs[-1]))

    def update_horizontal(self, xlim):
        """Check if dataset update is needed and update camera."""
        self.cursor.set_data(-self.camera.xstart + (xlim[0] + xlim[1]) * 0.5)
        self.camera.update_horizontal(xlim)

    def update_clim(self, cvals):
        """Update the color map limits."""
        for vis in self.spectro_viss:
            vis.clim = cvals


class VispyWindow(scene.SceneCanvas):
    """Create the vispy interface for data visualization."""

    def __init__(self, parent_win, **kwargs):
        """Init the window class."""
        super().__init__(keys="interactive", size=(1200, 600), show=True)

        self.unfreeze()
        self.grid_plots = self.central_widget.add_grid(margin=10)
        self.grid_plots.spacing = 10
        self._views = []
        self.parent_win = parent_win

        self.xaxis = scene.AxisWidget(
            orientation="bottom",
            axis_label="Time (s)",
            axis_font_size=12,
            tick_label_margin=15,
        )
        self.xaxis.height_max = 80
        # self.xlim = (0.0, 10.0)

    @property
    def xlim(self):
        """Min and max x range."""
        return self._xlim

    @xlim.setter
    def xlim(self, xlim):
        """Set the x lim."""
        self._xlim = xlim
        for vi in self._views:
            vi.update_horizontal(xlim)

        # update the cursor label
        self.cursor_pos.text = "{0:.2f}".format((xlim[0] + xlim[1]) * 0.5)

    def add_pyrline(self, sub_dat, sub_time, raw_dat=None, vtype="ephys", mapping=None):
        """Add a line plot that uses pyramid scaling."""
        if vtype == "ephys":
            axis_label = "Voltage (mV)"
        else:
            axis_label = "Voltage (V)"

        yaxis = scene.AxisWidget(
            orientation="left",
            axis_label=axis_label,
            axis_font_size=12,
            axis_label_margin=50,
            tick_label_margin=10,
        )
        yaxis.width_max = 80
        n_views = len(self._views)
        self.grid_plots.add_widget(yaxis, row=n_views, col=0)

        view = EPhysWidget(sub_dat, sub_time, raw_dat, vtype=vtype, mapping=mapping)
        self.grid_plots.add_widget(view, row=n_views, col=1)
        yaxis.link_view(view)
        self._views.append(view)

        if vtype == "ephys":
            self.parent_win.right_layout.addWidget(QtWidgets.QLabel("Channels"))
            self.check_median = QtWidgets.QCheckBox("median filter")
            self.check_median.stateChanged.connect(view.update_filter)
            self.parent_win.right_layout.addWidget(self.check_median)
            self.parent_win.right_layout.addWidget(QtWidgets.QLabel("Offset:"))
            combo = QtWidgets.QComboBox()
            offsets = ("0.05", "0.1", "0.2", "0.4", "0.8", "1.6")
            combo.addItems(offsets)
            combo.currentTextChanged.connect(view.update_offset)
            combo.setCurrentIndex(2)
            self.parent_win.right_layout.addWidget(combo)

    def add_spectro(self, spectros: zarr.hierarchy.Group, stype: str = "spectro"):
        """Add a spectrogram to the vispy window.

        Args:
            spectros (zarr.hierarchy.Group): the spectrogram
            stype (str): spectro or scalo
        """
        yaxis = scene.AxisWidget(
            orientation="left",
            axis_label="Frequency (Hz)",
            axis_font_size=12,
            axis_label_margin=50,
            tick_label_margin=10,
        )
        yaxis.width_max = 80
        n_views = len(self._views)

        # creating the view
        if stype == "spectro":
            view_spectro = SpectroWidget(spectros, self.parent_win.right_layout)
        elif stype == "scalo":
            view_spectro = ScaloWidget(spectros, self.parent_win.right_layout)
        else:
            logging.warning("Spectrogram type not supported: %s", stype)
            return

        self.grid_plots.add_widget(yaxis, row=n_views, col=0)
        self.grid_plots.add_widget(view_spectro, row=n_views, col=1)

        yaxis.link_view(view_spectro)
        self._views.append(view_spectro)

    def add_xaxis(self):
        """Add an x axis to the vispy window."""
        n_views = len(self._views)
        self.grid_plots.add_widget(self.xaxis, row=n_views, col=1)
        self.xaxis.link_view(self._views[0])

        # Add the cursor position text
        self.cursor_pos = scene.visuals.Text(
            str(0.0), pos=(50, 50), parent=self.xaxis, color="red"
        )
        # Add a spacer on the right column
        self.parent_win.right_layout.addStretch(1)
        self.xlim = (
            self._views[0].camera.rect.left,
            self._views[0].camera.rect.right,
        )

    def add_raster(
        self,
        spiketimes: npt.ArrayLike,
        orders: dict[str, dict[str, Any]],
        infos,
        sample_rate: float,
    ) -> None:
        """Add a raster to the vispy window.

        Args:
            spiketimes (npt.ArrayLike): the spike times
            orders (dict[str, dict[str, Any]]): the order dictionary
            infos (): to be defined...
            sample_rate (float): sample rate
        """
        # creating the view
        view_spikes = RasterWidget(
            spiketimes,
            self.parent_win.right_layout,
            orders,
            infos,
            sample_rate,
        )
        n_views = len(self._views)
        self.grid_plots.add_widget(view_spikes, row=n_views, col=1)

        self._views.append(view_spikes)
