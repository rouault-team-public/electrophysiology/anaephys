"""Functions performing downsampling and filtering."""
from __future__ import annotations

import logging
from typing import TYPE_CHECKING

import numpy as np
import tqdm
from scipy import signal

if TYPE_CHECKING:
    from typing import cast

    from numpy import typing as npt

    Afloat = npt.NDArray[np.float64]


def downsample(in_signal: Afloat, factor: int) -> Afloat:
    """Downsample a 1D or 2D signal.

    Args:
        in_signal (np.ndarray): input signal
        factor (np.integer): downsampling factor

    Returns:
        np.ndarray: output array

    Raises:
        TypeError: the number of input dimensions is wrong.
    """
    fcut = 1 / factor
    sos = signal.butter(4, fcut, output="sos")

    if in_signal.ndim == 1:
        return cast("Afloat", signal.sosfiltfilt(sos, in_signal)[::factor])
    elif in_signal.ndim == 2:
        out = np.empty(
            (in_signal.shape[0], (in_signal.shape[1] - 1) // factor + 1)
        )
        logging.info("Downsampling each channel by a factor %d", factor)
        for ichan in tqdm.trange(in_signal.shape[0]):
            out[ichan] = signal.sosfiltfilt(sos, in_signal[ichan])[::factor]
        return out

    raise TypeError("The input array must have 1 or 2 dimensions")


def remove50hz(in_signal: Afloat, fs: float) -> Afloat:
    """Remove the 50Hz and higher harmonics.

    Args:
        in_signal (np.ndarray): input signal
        fs (np.float): sampling frequency

    Returns:
        np.ndarray: output signal

    Raises:
        TypeError: wrong input dimension
    """
    # 50Hz filtering
    f0 = 50.0  # Frequency to be removed from signal (Hz)
    qa = 100.0  # Quality factor
    # Design notch filter
    b50, a50 = signal.iircomb(f0, qa, fs=fs)

    if in_signal.ndim == 1:
        return cast("Afloat", signal.filtfilt(b50, a50, in_signal))
    elif in_signal.ndim == 2:
        out = np.empty_like(in_signal)
        logging.info("Filtering each channel against 50Hz")
        for ichan in tqdm.trange(in_signal.shape[0]):
            out[ichan] = signal.filtfilt(b50, a50, in_signal[ichan])
        return out

    raise TypeError("The input array must have 1 or 2 dimensions")
