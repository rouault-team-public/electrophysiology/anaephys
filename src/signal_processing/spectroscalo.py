"""Compute spectrograms and scalograms."""
import logging

import numpy as np
import tqdm
from joblib import Parallel, delayed
from scipy import signal

SUBSAMP_SCALO = 32


def spectrogram(lfp, segment: int, sample_rate: float) -> tuple:
    """Compute the spectrogram with several windows."""
    spectro_sum = None
    freqs = None
    t_range = None
    logging.info("Computing the spectrogram for each channel")
    for i_chan in tqdm.trange(lfp.shape[0]):
        freqs, t_range, spectro = signal.spectrogram(
            lfp[i_chan],
            fs=sample_rate,
            window="hamming",
            nperseg=segment,
            noverlap=segment // 2,
        )
        if spectro_sum is None:
            spectro_sum = spectro
        else:
            spectro_sum += spectro
    return freqs, t_range, spectro_sum


def cwt_power(
    array: np.ndarray, widths: np.ndarray, omega0: float
) -> np.ndarray:
    """Perform the wavelet transform and output the power.

    Args:
        array (np.ndarray): Description
        widths (np.ndarray): Description
        omega0 (float): Description

    Returns:
        np.ndarray: the array containing the power for each frequency
    """
    cwt_out = signal.cwt(
        array, signal.morlet2, widths, w=omega0, dtype=np.csingle
    )
    subsamp = cwt_out[:, ::SUBSAMP_SCALO]
    return subsamp.real**2 + subsamp.imag**2


def scalogram(lfps, sample_rate: float):
    """Compute the scalogram."""
    # The scalograms are long to compute, it might be better to compute
    # them live!

    # We are following:
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.morlet2.html
    # Parameter defining the wavelet:
    omega0 = 10.0

    lfp_chan = lfps["channels/1"]
    tot_len = lfp_chan.shape[1]
    lfp_chan = lfp_chan[:, :tot_len]
    tss = lfps["timestamps/1"][:tot_len]

    # building a pow 2 array length
    ar_len = 2 ** (int(np.log2(tot_len)) + 1)

    n_chan = lfp_chan.shape[0]
    logging.info("Number of channels: %d", n_chan)
    # ar_volt = np.zeros((n_chan, ar_len))
    ar_volt = np.zeros((n_chan, ar_len))
    ar_volt[:, :tot_len] = lfp_chan

    # Decomposing in complex wavelet transform
    n_scales = 128
    freqs = np.logspace(
        0.0, 2.5, n_scales
    )  # frequencies between 1Hz and 300Hz
    # For molet2, we have:
    freq_lfp = (tss.size - 1) / (tss[-1] - tss[0])
    widths = omega0 * freq_lfp / (2 * np.pi * freqs)

    logging.info("Performing the wavelet transform in parallel")

    job_list = (
        delayed(cwt_power)(ar_volt[i_chan], widths, omega0)
        for i_chan in tqdm.trange(n_chan)
    )
    coefs_out = np.stack(Parallel(n_jobs=4)(job_list), axis=0)[
        :, :, : tot_len // SUBSAMP_SCALO
    ]
    logpower = np.log(np.mean(coefs_out, axis=0))

    return freqs, tss[::SUBSAMP_SCALO], logpower
