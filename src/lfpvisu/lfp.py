"""Compute LFPs and spectrograms."""
import argparse
import logging
import sys
from pathlib import Path

import numpy as np
import zarr
from signal_processing import downsampling, spectroscalo

# the standard cutoff frequency is apparently 1250Hz. To be checked!
# It should be better implemented (configurable)
FREQ_CUT = 1250  # in Hz


def compute_lfp(data_path: Path) -> None:
    """Compute the LFP for channels and subsample ADCs.

    Args:
        data_path (Path): the dataset folder.
    """
    logging.info("Computing the LFPs and subsampling the ADCs")

    raw_dat = None
    try:
        raw_dat = zarr.open(data_path / "raw/dataset.zarr", "r")
    except zarr.errors.PathNotFoundError:
        logging.exception("There is no zarr raw dataset.")
        return

    # We need to round the sampling rate to avoid rounding errors below
    sample_rate = round(raw_dat.attrs["sample_rate"])
    n_chan = raw_dat["channels"].shape[0]
    tot_len = raw_dat["channels"].shape[1]
    raw_chans = raw_dat["channels"]

    if "conv_factor" in raw_dat.attrs:
        conv_factor = np.array(raw_dat.attrs["conv_factor"])
    else:
        conv_factor = np.array([1.0])

    down_fact = round(sample_rate / FREQ_CUT)

    zarr_out = zarr.open(data_path / "processed/ephys.zarr", mode="a")

    logging.info("Computing the timestamps")
    tss = {}
    tss["1"] = (np.arange(tot_len) / sample_rate)[::down_fact]
    tss["16"] = tss["1"][::16]
    tss["256"] = tss["16"][::16]

    logging.info("Compute the LFP for each channel")
    sample_rate1 = (tss["1"].size - 1) / (tss["1"][-1] - tss["1"][0])

    lfps = {}
    lfps["1"] = (
        downsampling.downsample(raw_chans, down_fact) * conv_factor[:, np.newaxis]
    )
    logging.info("Downsampled sampling rate: %f", sample_rate1)
    # The sampling rate must be a multiple of 50 Hz
    srate_approx = round(sample_rate1 / 50) * 50
    lfps["1"] = downsampling.remove50hz(lfps["1"], srate_approx)
    lfps["16"] = downsampling.downsample(lfps["1"], 16)
    lfps["256"] = downsampling.downsample(lfps["16"], 16)

    zarr_lfp = zarr_out.create_group("lfps", overwrite=True)
    zarr_chans = zarr_lfp.create_group("channels", overwrite=True)
    zarr_ts = zarr_lfp.create_group("timestamps", overwrite=True)

    for res in ("1", "16", "256"):
        zarr_chans.array(res, data=lfps[res], overwrite=True, chunks=(n_chan, 16386))
        zarr_ts.array(res, data=tss[res], dtype=np.float64, overwrite=True)

    if "adcs" in raw_dat:
        raw_adcs = raw_dat["adcs"]
        n_adc = raw_dat["adcs"].shape[0]

        if "conv_factor_adc" in raw_dat.attrs:
            conv_factor_adc = np.array(raw_dat.attrs["conv_factor_adc"])
        else:
            conv_factor_adc = np.array([1.0])

        logging.info("ADC subsampling")
        adcs = {}
        adcs["1"] = (
            downsampling.downsample(raw_adcs, down_fact)
            * conv_factor_adc[:, np.newaxis]
        )
        adcs["16"] = downsampling.downsample(adcs["1"], 16)
        adcs["256"] = downsampling.downsample(adcs["16"], 16)

        zarr_adcs = zarr_lfp.create_group("adcs", overwrite=True)

        zarr_adcs.array(
            res,
            data=adcs[res],
            dtype=np.float32,
            overwrite=True,
            chunks=(n_adc, 131072),
        )


def compute_spectrograms(data_path: Path) -> None:
    """Compute spectrograms for several windows.

    Args:
        data_path (Path): the dataset folder.
    """
    logging.info("Computing the spectrograms")
    try:
        zarr_inout = zarr.open_group(data_path / "processed/ephys.zarr", "a")
    except zarr.errors.GroupNotFoundError:
        logging.warning("The LFP signal is not found, aborting spectrogram generation.")
        return

    zarr_spectro = zarr_inout.create_group("spectro", overwrite=True)
    for seg in (256, 2048):
        zarr_seg = zarr_spectro.create_group(str(seg), overwrite=True)
        freqs, t_range, spectro = spectroscalo.spectrogram(
            zarr_inout["lfps/channels/1"], seg, FREQ_CUT
        )
        zarr_seg.array("freqs", data=freqs, dtype=np.float32, overwrite=True)
        zarr_seg.array("times", data=t_range, dtype=np.float32, overwrite=True)
        zarr_seg.array(
            "logspectro",
            data=np.log(spectro),
            dtype=np.float32,
            overwrite=True,
            chunks=(freqs.size, 1024 * 1024 // freqs.size),
        )


def compute_scalogram(data_path: Path) -> None:
    """Compute scalogram.

    Args:
        data_path (Path): the dataset folder.
    """
    logging.info("Computing the scalogram")
    try:
        zarr_inout = zarr.open_group(data_path / "processed/ephys.zarr", "a")
    except zarr.errors.GroupNotFoundError:
        logging.warning("The LFP signal is not found, aborting spectrogram generation.")
        return

    zarr_scalo = zarr_inout.create_group("scalo", overwrite=True)
    freqs, t_range, scalo = spectroscalo.scalogram(zarr_inout["lfps"], FREQ_CUT)
    zarr_scalo.array("freqs", data=freqs, dtype=np.float32, overwrite=True)
    zarr_scalo.array("times", data=t_range, dtype=np.float32, overwrite=True)
    zarr_scalo.array(
        "logscalo",
        data=np.log(scalo),
        dtype=np.float32,
        overwrite=True,
        chunks=(freqs.size, 1024 * 1024 // freqs.size),
    )


def main() -> int:
    """Start the script."""
    # Note that if the root logger is already configured, the following
    # function does nothing, which is what we want.
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Compute the LFP and spectrogram")
    parser.add_argument(
        "folder",
        help="Dataset folder",
    )
    parser.add_argument(
        "--lfp",
        action="store_true",
        help="Compute the lfp and downsample the ADCs",
    )
    parser.add_argument(
        "--spectro",
        action="store_true",
        help="Compute the spectrograms",
    )
    parser.add_argument(
        "--scalo",
        action="store_true",
        help="Compute the scalograms",
    )
    args = parser.parse_args()
    data_path = Path(args.folder)

    if args.lfp:
        compute_lfp(data_path)

    if args.spectro:
        compute_spectrograms(data_path)

    if args.scalo:
        compute_scalogram(data_path)

    return 0


if __name__ == "__main__":
    # execute only if run as a script
    sys.exit(main())
