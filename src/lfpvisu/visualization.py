"""Plot ephys curves with vispy."""
import argparse
import logging
import sys
from importlib import resources
from pathlib import Path
from typing import Any

import h5py
import mappings
import numpy as np
import yaml
import zarr
from PySide6 import QtWidgets
from qtvispy import interface


def import_orders(spike_times: dict[str, Any], data_path: Path) -> dict[str, list[str]]:
    """Import a cell order.

    Args:
        spike_times (dict[str, Any]): spike times
        data_path (Path): dataset path

    Returns:
        dict[str, list[str]]: the dictionary containing the orders

    """
    orders: dict[str, list[str]] = {}
    orders["default"] = [f"temp_{i_c}" for i_c in np.arange(len(spike_times))]
    infos = {}
    infos["default"] = None
    umap_orders = None
    try:
        umap_orders = zarr.open_group(data_path / "processed/umap/umap.zarr")
    except zarr.errors.GroupNotFoundError:
        logging.warning("No umap cell order found!")
    if umap_orders is not None:
        logging.info("Opening the cell orders")
        for cond, order in umap_orders.items():
            logging.info("Condition: %s", cond)
            u_ord = order["order"][:]
            u_tun = order["tuned"][:]
            u_info = order["info"][:]
            cells = u_ord[u_tun[u_ord]]
            list_cells = [f"temp_{i_c}" for i_c in cells]
            orders[f"umap_{cond}"] = list_cells
            infos[f"umap_{cond}"] = u_info[cells]

    return orders, infos


def read_dataset(data_path: Path) -> dict[str, Any]:
    """Put all the datasets into a dictionary.

    Args:
        data_path (Path): dataset folder


    Returns:
        dict[str, Any]: the dataset dictionary


    """
    logging.info("Opening the dataset: %s", data_path.name)

    dataset = {}

    zarr_in = zarr.open_group(data_path / "processed/ephys.zarr", "r")

    logging.info("Opening the LFPs")
    try:
        dataset["lfps"] = zarr_in["lfps"]
    except KeyError:
        logging.warning("LFPs are not present")

    logging.info("Opening the raw EPhys signal")
    try:
        dataset["raw"] = zarr.open_group(data_path / "raw/dataset.zarr", "r")
    except zarr.errors.GroupNotFoundError:
        logging.warning("raw signals are not present")

    try:
        dataset["spectro"] = zarr_in["spectro"]
    except KeyError:
        logging.warning("Spectrograms are not present")

    try:
        dataset["scalo"] = zarr_in["scalo"]
    except KeyError:
        logging.warning("Scalograms are not present")

    # load the spikes
    try:
        sorting_fold = data_path / "processed/spike-sorting/dataset"
        merged = list(sorting_fold.glob("*.result-merged.hdf5"))
        if len(merged) != 1:
            logging.warning(
                "Several or none .result-merged.hdf5 file detecting. Aborting!"
            )
        else:
            with h5py.File(merged[0], "r") as spike_in:
                dataset["spiketimes"] = {}
                for temp in spike_in["spiketimes"].items():
                    dataset["spiketimes"][temp[0]] = temp[1][:]
    except FileNotFoundError:
        logging.exception("No spikes found!")

    if dataset.get("spiketimes") is not None:
        logging.info("Loading the spike times")
        orders, infos = import_orders(dataset["spiketimes"], data_path)
        dataset["orders"] = orders
        dataset["infos"] = infos

    if dataset.get("lfps") is not None:
        lfps = dataset["lfps"]
        if "channels" in lfps:
            logging.info("LFPs shape: %s", dataset["lfps"]["channels/1"].shape)
        if "adcs" in lfps:
            logging.info("ADCs shape: %s", dataset["lfps"]["adcs/1"].shape)

    return dataset


def main() -> int:
    """Start the script."""
    # Note that if the root logger is already configured, the following
    # function does nothing, which is what we want.
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument("folder", help="folder containing the dataset")
    parser.add_argument("--mapping", help=".yaml mapping file, optional")
    args = parser.parse_args()

    data_path = Path(args.folder)

    dataset = read_dataset(data_path)

    # logging.info("Creating the vispy window")
    app = QtWidgets.QApplication(sys.argv)
    main_window = interface.MainWindow()

    # load the mapping
    if args.mapping is not None:
        with open(Path(args.mapping), "r") as f_map:
            yaml_map = yaml.safe_load(f_map)
            mapping = np.array(yaml_map["mapping"], dtype=int)
    else:
        mapping = None
    # else:
    #     yaml_file = resources.open_text(mappings, "buzsaki64.yaml")
    #     yaml_map = yaml.safe_load(yaml_file)
    #     if (data_path / "raw/oephys").is_dir():
    #         logging.info("Using headstage and vertical mapping for OpenEPhys datasets")
    #         mapping1 = np.array(yaml_map["vertical"], dtype=int)
    #         mapping0 = np.array(yaml_map["initial"], dtype=int)
    #         mapping = mapping0[mapping1]
    #     else:
    #         logging.info("Using only the vertical mapping for nwb datasets")
    #         mapping = np.array(yaml_map["vertical"], dtype=int)

    if dataset.get("lfps") is not None:
        lfps = dataset["lfps"]
        if "channels" in lfps:
            main_window.vispy_canvas.add_pyrline(
                lfps["channels"],
                lfps["timestamps"],
                dataset["raw"],
                vtype="ephys",
                mapping=mapping,
            )
        if "adcs" in lfps:
            main_window.vispy_canvas.add_pyrline(
                lfps["adcs"],
                lfps["timestamps"],
                dataset["raw"],
                vtype="ephys",
                mapping=mapping,
            )

    if dataset.get("spectro") is not None:
        main_window.vispy_canvas.add_spectro(dataset["spectro"])

    if dataset.get("scalo") is not None:
        main_window.vispy_canvas.add_spectro(dataset["scalo"], stype="scalo")

    if dataset.get("spiketimes") is not None:
        sample_rate = dataset["raw"].attrs["sample_rate"]
        main_window.vispy_canvas.add_raster(
            dataset["spiketimes"],
            dataset["orders"],
            dataset["infos"],
            sample_rate,
        )

    main_window.vispy_canvas.add_xaxis()

    logging.info("Running the app")
    main_window.show()
    app.exec()

    return 0


if __name__ == "__main__":
    # execute only if run as a script
    sys.exit(main())
