"""diverse conversion scripts."""
from __future__ import annotations

import argparse
import json
import logging
import sys
from pathlib import Path
from typing import TYPE_CHECKING

import h5py
import numcodecs
import numpy as np
import yaml
import zarr
from defusedxml import ElementTree as ETree
from matplotlib import pyplot as plt
from numpy import typing as npt
from openephys import OpenEphys, continuous_props
from tqdm import tqdm

from lfpvisu import allego_file_reader as allego

if TYPE_CHECKING:
    from typing import Any

    Aint16 = npt.NDArray[np.int16]
    Aint = npt.NDArray[np.int64]
    Afloat32 = npt.NDArray[np.float32]
    Afloat64 = npt.NDArray[np.float64]


def oephys_to_zarr(data_path: Path) -> None:
    """Load the dataset into an array and save to .zarr."""
    props: dict[str, Any] = {}
    logging.info("Loading the dataset")
    if (data_path / "raw/oephys/Continuous_Data.openephys").is_file():
        logging.info("Continuous file format detected")
        props = continuous_props.extract_prop(data_path / "raw/oephys")
        # Extracting the channels
        n_chan = props["n_chan"]
        chan_array = np.zeros((n_chan, props["tot_len"]), dtype=np.int16)
        conv_factor = np.zeros(n_chan)

        logging.info("Converting the channels")
        for i_f, fname in enumerate(tqdm(props["chan_files"])):
            oephys_f = OpenEphys.load(
                str(data_path / "raw/oephys" / fname), dtype=np.int16
            )
            chan_array[i_f] = oephys_f["data"]
            conv_factor[i_f] = oephys_f["header"]["bitVolts"]

        # Extracting the adcs
        logging.info("Converting the ADCs")
        n_adc = props["n_adc"]
        adc_array = np.zeros((n_adc, props["tot_len"]), dtype=np.int16)
        conv_factor_adc = np.zeros(n_adc)
        for i_adc, adc_fname in enumerate(tqdm(props["adc_files"])):
            oephys_f = OpenEphys.load(
                str(data_path / "raw/oephys" / adc_fname), dtype=np.int16
            )
            adc_array[i_adc] = oephys_f["data"]
            conv_factor_adc[i_adc] = oephys_f["header"]["bitVolts"]

    elif (data_path / "raw/oephys/structure.oebin").is_file():
        logging.info("Raw binary file format detected")
        info = json.load((data_path / "raw/oephys/structure.oebin").open("r"))
        n_rec = len(info["continuous"])
        if n_rec == 0:
            logging.error("No recording detected")
            return
        elif n_rec > 1:
            logging.warning(
                "More than one recording detected: %d. Analyzing the first one",
                n_rec,
            )

        record = info["continuous"][0]
        logging.info("Sampling rate: %f", record["sample_rate"])
        props["sample_rate"] = record["sample_rate"]
        logging.info("Number of channels: %d", record["num_channels"])
        n_chan = 0
        n_adc = 0
        for chan in record["channels"]:
            if chan["channel_name"][:2] == "CH":
                n_chan += 1
            elif chan["channel_name"][:3] == "ADC":
                n_adc += 1
            else:
                logging.warning("Channel type not recognized: %s", chan["channel_name"])
        n_chan_tot = len(record["channels"])

        data = np.memmap(
            data_path / "raw/oephys/continuous.dat", mode="r", dtype="int16"
        )
        samples = data.reshape((len(data) // n_chan_tot, n_chan_tot))
        props["tot_len"] = samples.shape[0]

        chan_array = np.zeros((n_chan, props["tot_len"]), dtype=np.int16)
        conv_factor = np.zeros(n_chan)
        adc_array = np.zeros((n_adc, props["tot_len"]), dtype=np.int16)
        conv_factor_adc = np.zeros(n_adc)

        logging.info("Converting the channels and adcs")
        i_chan = 0
        i_adc = 0
        for i_chantot, chan in enumerate(record["channels"]):
            if chan["channel_name"][:2] == "CH":
                chan_array[i_chan] = samples[:, i_chantot]
                conv_factor[i_chan] = chan["bit_volts"]
                i_chan += 1
            elif chan["channel_name"][:3] == "ADC":
                adc_array[i_adc] = samples[:, i_chantot]
                conv_factor_adc[i_adc] = chan["bit_volts"]
                i_adc += 1

    props["conv_factor"] = tuple(conv_factor)
    props["conv_factor_adc"] = tuple(conv_factor_adc)

    write_array_zarr(chan_array, adc_array, props, data_path)


def read_xml_metadata(xml_filepath: Path) -> dict[str, Any] | None:
    """Read the .xml metadata for ndManager.

    Args:
        xml_filepath: file path

    Returns:
        metadata dictionary
    """
    try:
        tree = ETree.parse(xml_filepath)
        root = tree.getroot()
    except ETree.ParseError:
        logging.exception("Invalid XML file: %s", xml_filepath)
        return None

    metadata: dict[str, Any] = {}
    try:
        metadata["n_channels"] = int(root.find("./acquisitionSystem/nChannels").text)
        n_bits = int(root.find("./acquisitionSystem/nBits").text)
        ampli = float(root.find("./acquisitionSystem/amplification").text)
        metadata["n_bits"] = n_bits
        v_range = float(root.find("./acquisitionSystem/voltageRange").text)
        metadata["sampling_rate"] = float(
            root.find("./acquisitionSystem/samplingRate").text
        )
        # We add a 1e6 factor below because the amplified channels are in uV
        metadata["conv_factor"] = 1e6 * v_range / 2 ** (n_bits - 1) / ampli
        metadata["conv_factor_adc"] = 1.0
    except AttributeError:
        logging.exception("Missing metadata fields in XML file: %s", xml_filepath)
        return None

    return metadata


def read_ndmdat(datasource_path: Path, metadata: dict[str, Any]) -> Aint16:
    """Read a raw ndmanager .dat file.

    Args:
        datasource_path: file path
        metadata: dictionary containing the sampling rate

    Returns:
        the whole array
    """
    fs = metadata["sampling_rate"]

    try:
        with datasource_path.open("rb") as fid:
            raw_sig_array = np.fromfile(
                fid,
                dtype=np.int16,
            )
    except ValueError:
        logging.exception(
            "could not load signal array data from file %s", datasource_path.name
        )

    return np.reshape(
        raw_sig_array,
        (-1, metadata["n_channels"]),
    ).T


def ndmdat_to_zarr(data_path: Path) -> None:
    """Load the dataset into an array and save to .zarr."""
    logging.info("Loading the dataset")

    # Extracting the channels
    # Looking for the metadata
    xmlfiles = list((data_path / "raw/ndmdat").glob("*.xml"))
    if len(xmlfiles) == 0:
        logging.error('No .xml file found in the "raw/ndmdat" sub-folder')
        return
    if len(xmlfiles) > 1:
        logging.error("More than one dataset file found")
        return

    logging.info("Analyzing dataset: %s", xmlfiles[0].stem.rstrip(".xml"))

    xmlmeta = read_xml_metadata(xmlfiles[0])
    if xmlmeta is None:
        return

    n_channels = xmlmeta["n_channels"]
    sampling_rate = xmlmeta["sampling_rate"]
    conv_factor = xmlmeta["conv_factor"] * np.ones(n_channels)

    logging.info("Total number of channels: %d", n_channels)
    logging.info("Sampling frequency: %fHz", sampling_rate)

    props = {
        "sample_rate": sampling_rate,
        "conv_factor": tuple(conv_factor),
    }

    all_chans = read_ndmdat(xmlfiles[0].with_suffix(".dat"), xmlmeta)

    chan_array = all_chans

    write_array_zarr(chan_array, None, props, data_path)


def xdat_to_zarr(data_path: Path) -> None:
    """Load the dataset into an array and save to .zarr."""
    logging.info("Loading the dataset")
    # Extracting the channels
    # Looking for the metadata
    jsonfiles = list((data_path / "raw/xdat").glob("*.xdat.json"))
    if len(jsonfiles) == 0:
        logging.error('No .xdat.json file found in the "raw/xdat" sub-folder')
        return
    elif len(jsonfiles) > 1:
        logging.error("More than one dataset file found")
        return
    else:
        logging.info("Analyzing dataset: %s", jsonfiles[0].stem.rstrip(".xdat.json"))

    xdat_filepath = jsonfiles[0].parent / jsonfiles[0].stem.rstrip(".xdat.json")

    meta = allego.read_allego_xdat_metadata(xdat_filepath)
    status = meta["status"]
    n_prichans = status["signals"]["pri"]
    n_auxchans = status["signals"]["aux"]
    logging.info("Total number of channels: %d", status["signals"]["total"])
    logging.info("Number of primary (amplified) channels: %d", n_prichans)
    logging.info("Number of auxiliary channels: %d", n_auxchans)
    logging.info("Recording duration: %fs", status["dur"])
    logging.info("Sampling frequency: %fHz", status["samp_freq"])

    props = {"sample_rate": status["samp_freq"]}

    all_chans, timest, times = allego.read_allego_xdat_all_signals(xdat_filepath)

    chan_array = all_chans[:n_prichans]
    adc_array = all_chans[n_prichans : n_prichans + n_auxchans]

    write_array_zarr_float(chan_array, adc_array, props, data_path)


def oephy_nwb_to_zarr(f_in: h5py.File, data_path: Path) -> None:
    continuous_rec = f_in["acquisition/timeseries/recording1/continuous"]
    processors = tuple(continuous_rec.keys())
    logging.info("Using the first 1/%d: %s", len(processors), processors[0])
    proc = continuous_rec[processors[0]]

    timestamps = proc["timestamps"]
    diffs = np.diff(timestamps)
    t_length = timestamps[-1] - timestamps[0]
    tot_len = timestamps.size
    sample_rate = (tot_len - 1) / t_length
    logging.info("Diagnostics of the timestamps (aberrant timestamps)")
    lowts = diffs < 1 / 2 / sample_rate
    hights = diffs > 2 / sample_rate
    logging.info(
        "low/high vals, indices: %s %s",
        diffs[lowts | hights],
        np.nonzero(lowts | hights)[0],
    )

    logging.info("mean sample rate: %f", sample_rate)

    # n_chan is hard-coded for .nwb...
    n_chan = 64
    conv_factor = proc["data"].attrs["conversion"] * np.ones(n_chan)
    # conv_factor_adc is hard-coded for .nwb since this is not stored in
    # the .nwb version 1...
    n_adc = 8
    conv_factor_adc = 0.00015258 * np.ones(n_adc)
    logging.info("Channel shape: %s", proc["data"].shape)
    logging.info("Channel chunks: %s", proc["data"].chunks)
    props = {
        "sample_rate": sample_rate,
        "conv_factor": tuple(conv_factor),
        "conv_factor_adc": tuple(conv_factor_adc),
    }
    write_array_zarr(
        proc["data"][:, :n_chan].T,
        proc["data"][:, n_chan:].T,
        props,
        data_path,
    )


def nnexus_nwb_to_zarr(f_in: h5py.File, data_path: Path) -> None:
    electrodes = f_in["general/extracellular_ephys/electrodes"]
    ports = electrodes["port"][:]
    logging.debug("Ports: %s", ports)

    select_ephys = ports == b"A"
    select_aux = ports == b"AUX"
    logging.info("Selecting port A")

    cont_signal = f_in["acquisition/continuous_signals"]
    sample_rate = cont_signal["starting_time"].attrs["rate"]

    data_v = f_in["acquisition/continuous_signals/data"]

    ephys = data_v[:, select_ephys]
    aux_signal = data_v[:, select_aux]

    n_chan = ephys.shape[1]
    conv_factor = data_v.attrs["conversion"] * 1e6 * np.ones(n_chan)
    n_adc = aux_signal.shape[1]
    # conversion are erroneous for ADCs...
    conv_factor_adc = np.ones(n_adc)
    props = {
        "sample_rate": sample_rate,
        "conv_factor": tuple(conv_factor),
        "conv_factor_adc": tuple(conv_factor_adc),
    }

    write_array_zarr(ephys.T, aux_signal.T, props, data_path)


def nwb_to_zarr(data_path: Path) -> None:
    """Load the dataset into an array and save to .zarr."""
    logging.info("Trying to open the dataset in .nwb format.")
    nwb_files = list((data_path / "raw/nwb").glob("*.nwb"))
    logging.info("Converting %s", nwb_files[0].name)

    with h5py.File(nwb_files[0], "r") as f_in:
        if "acquisition/timeseries/recording1/continuous" in f_in:
            logging.info("OpenEphys nwb format detected")
            oephy_nwb_to_zarr(f_in, data_path)
        elif "acquisition/continuous_signals/data" in f_in:
            logging.info("NeuroNexus nwb format detected")
            nnexus_nwb_to_zarr(f_in, data_path)
        else:
            logging.error("nwb format not recognized")


def check_ts_nwb(data_path: Path) -> None:
    """Load the dataset and check the timestamps."""
    logging.info("Trying to open the dataset in .nwb format.")
    with h5py.File(data_path / "raw/nwb/experiment_1.nwb", "r") as f_in:
        continuous_rec = f_in["acquisition/timeseries/recording1/continuous"]
        processors = tuple(continuous_rec.keys())
        logging.info("Using the first 1/%d: %s", len(processors), processors[0])
        proc = continuous_rec[processors[0]]

        timestamps = proc["timestamps"][:]

    fig, ax = plt.subplots()
    subsamp = 64
    ax.plot(np.diff(timestamps[::subsamp]) / subsamp * 25000)
    ax.set_ylim(0.5, 1.5)
    plt.show()


def write_array_zarr(
    chan_array: Aint16, adc_array: Aint16 | None, props: dict[str, Any], data_path: Path
) -> None:
    """Write channel and ADC arrays to zarr.

    Args:
        chan_array (np.ndarray): the channel array
        adc_array (np.ndarray): the adc array
        props (dict): signal properties
        data_path (Path): dataset folder
    """
    logging.info("Compressing the dataset to zarr")
    zarr_out = zarr.open(data_path / "raw/dataset.zarr", mode="a")
    zarr_out.attrs["sample_rate"] = props["sample_rate"]
    zarr_out.attrs["conv_factor"] = props["conv_factor"]

    compressor = numcodecs.Blosc(
        cname="zstd", clevel=3, shuffle=numcodecs.Blosc.BITSHUFFLE
    )
    zarr_out.array(
        "channels",
        data=chan_array,
        chunks=(4, 262144),
        overwrite=True,
        compressor=compressor,
        dtype=np.int16,
    )
    if adc_array is not None:
        zarr_out.attrs["conv_factor_adc"] = props["conv_factor_adc"]
        zarr_out.array(
            "adcs",
            data=adc_array,
            chunks=(4, 262144),
            overwrite=True,
            compressor=compressor,
            dtype=np.int16,
        )


def write_array_zarr_float(
    chan_array: Afloat32, adc_array: Afloat32, props: dict[str, Any], data_path: Path
) -> None:
    """Write channel and ADC arrays to zarr.

    Args:
        chan_array (np.ndarray): the channel array
        adc_array (np.ndarray): the adc array
        props (dict): signal properties
        data_path (Path): dataset folder
    """
    logging.info("Compressing the dataset to zarr")
    zarr_out = zarr.open(data_path / "raw/dataset.zarr", mode="a")
    zarr_out.attrs["sample_rate"] = props["sample_rate"]

    compressor = numcodecs.Blosc(
        cname="zstd", clevel=3, shuffle=numcodecs.Blosc.BITSHUFFLE
    )
    zarr_out.array(
        "channels",
        data=chan_array,
        chunks=(4, 262144),
        overwrite=True,
        compressor=compressor,
    )
    zarr_out.array(
        "adcs",
        data=adc_array,
        chunks=(4, 262144),
        overwrite=True,
        compressor=compressor,
    )


def zarr_to_raw(data_path: Path, coordinates: Aint16 | None) -> None:
    """Load the dataset into an array and save to .raw.

    Args:
        data_path (Path): the dataset folder.
        coordinates (npt.NDArray[np.integer]): The regions defining each
            dataset.
    """
    logging.info("Converting to raw for spike sorting")

    raw_dat = None
    try:
        raw_dat = zarr.open(data_path / "raw/dataset.zarr", "r")
    except zarr.errors.PathNotFoundError:
        logging.exception("There is no zarr raw dataset.")
        return

    chans = raw_dat["channels"]
    logging.info("The full dataset has shape: %s", chans.shape)

    (data_path / "processed/spike-sorting").mkdir(parents=True, exist_ok=True)
    if coordinates is None:
        logging.info("Converting the whole dataset")
        chans[:].T.tofile(data_path / "processed/spike-sorting/dataset.dat")
    else:
        for i_reg, reg in enumerate(coordinates):
            logging.info("Converting region %s", reg)
            chans[:, reg[0] : reg[1]].T.tofile(
                data_path / f"processed/spike-sorting/dataset-{i_reg}.dat"
            )


def main() -> int:
    """Start the script."""
    # Note that if the root logger is already configured, the following
    # function does nothing, which is what we want.
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Convert EPhys dataset formats")
    parser.add_argument(
        "folder",
        help="Dataset folder",
    )
    parser.add_argument(
        "--oephystozarr",
        action="store_true",
        help="Convert the openephys dataset to zarr",
    )
    parser.add_argument(
        "--nwbtozarr",
        action="store_true",
        help="Convert the nwb dataset to zarr",
    )
    parser.add_argument(
        "--xdattozarr",
        action="store_true",
        help="Convert the xdat dataset to zarr",
    )
    parser.add_argument(
        "--ndmdattozarr",
        action="store_true",
        help="Convert the ndManager .dat dataset to zarr",
    )
    parser.add_argument(
        "--check-ts",
        action="store_true",
        help="Check the timestamps continuity",
    )
    parser.add_argument(
        "--zarrtoraw",
        action="store_true",
        help="Convert the zarr dataset to raw",
    )
    args = parser.parse_args()
    data_path = Path(args.folder)

    if args.oephystozarr:
        logging.info("Converting the openephys dataset")
        oephys_to_zarr(data_path)

    if args.nwbtozarr:
        logging.info("Converting the nwb dataset")
        nwb_to_zarr(data_path)

    if args.xdattozarr:
        logging.info("Converting the nwb dataset")
        xdat_to_zarr(data_path)

    if args.ndmdattozarr:
        logging.info("Converting the ndmanager .dat dataset")
        ndmdat_to_zarr(data_path)

    if args.check_ts:
        logging.info("Checking the timestamps")
        check_ts_nwb(data_path)

    if args.zarrtoraw:
        logging.info("Generating a raw dataset for spike sorting")
        regions = None
        try:
            with (data_path / "sorting-regions.yaml").open("r") as reg_in:
                regions = yaml.safe_load(reg_in)
        except FileNotFoundError:
            logging.warning("Will generate the raw dataset in only one shunk.")
        zarr_to_raw(data_path, regions)

    return 0


if __name__ == "__main__":
    # execute only if run as a script
    sys.exit(main())
